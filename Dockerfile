FROM python:3.8-buster

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN pip3 install pipenv && apt update && apt install make

COPY Pipfile Pipfile
RUN pipenv lock --keep-outdated --requirements > requirements.txt && pip3 install -r requirements.txt

COPY . .