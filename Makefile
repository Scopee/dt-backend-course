migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

prod:
	python src/manage.py runserver 0.0.0.0:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

runbot:
	python src/manage.py run_bot

runbot_webhook:
	python src/manage.py run_bot_webhooks

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

up:
	docker-compose up -d

down:
	docker-compose down

make_migrations_docker:
	docker-compose run web python src/manage.py makemigrations

migrate_docker:
	docker-compose run web python src/manage.py migrate

createsuperuser_docker:
	docker-compose run web python src/manage.py createsuperuser

collectstatic_docker:
	docker-compose run web python src/manage.py collectstatic --no-input

build:
	docker-compose build

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

ci_lint:
	docker-compose run web /bin/sh -c "make check_lint"

test:
	docker-compose run web /bin/sh -c "pytest src/tests --disable-warnings"
