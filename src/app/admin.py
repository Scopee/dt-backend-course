from django.contrib import admin

from app.internal.admin.account import AdminAccount
from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.card import Card
from app.internal.admin.user import AdminUserInfo

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
