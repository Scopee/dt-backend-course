from django.contrib import admin

from app.internal.models.card import Card


@admin.register(Card)
class AdminCard(admin.ModelAdmin):
    fields = ['account', 'card_number']
