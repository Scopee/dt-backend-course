from django.contrib import admin

from app.internal.models.user import UserInfo


@admin.register(UserInfo)
class AdminUserInfo(admin.ModelAdmin):
    fields = ['name', 'phone_number']
