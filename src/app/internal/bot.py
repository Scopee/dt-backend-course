import logging

from telegram.ext import CommandHandler, Dispatcher, Filters, MessageHandler, \
    Updater

from app.internal.transport.bot.filters.argument_filter import ArgumentFilter
from app.internal.transport.bot.filters.phone_filled_filter import \
    IsPhoneFilled
from app.internal.transport.bot.filters.user_registered_filter import \
    UserRegisteredFilter
from app.internal.transport.bot.handlers import (
    account_callback,
    add_user_to_favorite,
    card_balance_callback,
    cards_callback,
    contact_button_callback,
    get_favorite_users,
    get_transaction_by_account_or_card,
    get_users_interacted,
    me_callback,
    remove_user_from_favorites,
    send_money_callback,
    set_phone_callback,
    start_callback,
    show_help
)

logger = logging.getLogger(__name__)


class BotListener:

    @staticmethod
    def run_pooling(token: str) -> None:
        if not token:
            logger.error("No token provided. Exit.")
            return
        updater = Updater(token, use_context=True)

        dp = updater.dispatcher
        dp = BotListener.setup(dp)

        updater.start_polling(bootstrap_retries=1)
        logger.info('Start pooling...')

    @staticmethod
    def run_webhook(token: str, webhook_ip: str, webhook_port: int,
                    url_path: str, url: str) -> None:
        if not token:
            logger.error("No token provided. Exit.")
            return
        updater = Updater(token, use_context=True)

        dp = updater.dispatcher
        dp = BotListener.setup(dp)
        updater.start_webhook(
            listen=webhook_ip,
            port=webhook_port,
            url_path=url_path,
            webhook_url=url + url_path,
        )
        updater.idle()
        logger.info('Start webhook...')

    @staticmethod
    def setup(dp: Dispatcher) -> Dispatcher:
        dp.add_handler(CommandHandler("start", start_callback))
        dp.add_handler(CommandHandler("set_phone", set_phone_callback,
                                      filters=UserRegisteredFilter()))
        dp.add_handler(
            MessageHandler(Filters.contact, contact_button_callback))
        dp.add_handler(
            CommandHandler("me", me_callback,
                           filters=IsPhoneFilled() & UserRegisteredFilter()))
        dp.add_handler(CommandHandler("accounts", account_callback,
                                      filters=(IsPhoneFilled()
                                               & UserRegisteredFilter())))
        dp.add_handler(
            CommandHandler("cards", cards_callback,
                           filters=IsPhoneFilled() & UserRegisteredFilter()))
        dp.add_handler(CommandHandler("card_balance", card_balance_callback,
                                      filters=(UserRegisteredFilter()
                                               & IsPhoneFilled()
                                               & ArgumentFilter(1,
                                                                'Please enter card number'))))
        dp.add_handler(CommandHandler("send_money", send_money_callback,
                                      filters=(UserRegisteredFilter()
                                               & IsPhoneFilled()
                                               & ArgumentFilter(3,
                                                                'Please enter account from, to and amount'))))
        dp.add_handler(CommandHandler("favorites", get_favorite_users,
                                      filters=(UserRegisteredFilter()
                                               & IsPhoneFilled())))
        dp.add_handler(CommandHandler("add_to_favorites", add_user_to_favorite,
                                      filters=(UserRegisteredFilter()
                                               & IsPhoneFilled()
                                               & ArgumentFilter(1,
                                                                'Please enter username'))))
        dp.add_handler(CommandHandler("get_transactions",
                                      get_transaction_by_account_or_card,
                                      filters=(UserRegisteredFilter()
                                               & IsPhoneFilled()
                                               & ArgumentFilter(1,
                                                                'Please enter account or card number'))))
        dp.add_handler(
            CommandHandler("get_users_interacted", get_users_interacted,
                           filters=UserRegisteredFilter() & IsPhoneFilled()))
        dp.add_handler(
            CommandHandler("remove_from_favorites", remove_user_from_favorites,
                           filters=(UserRegisteredFilter()
                                    & IsPhoneFilled() & ArgumentFilter(1,
                                                                       'Please enter username'))))
        dp.add_handler(CommandHandler("help", show_help))
        logger.info('Add commands to bot dispatcher')
        return dp
