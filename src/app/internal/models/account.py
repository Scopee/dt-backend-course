from django.core.exceptions import ValidationError
from django.db import models

from app.internal.models.user import UserInfo


def is_account_number_length_valid(account_number: str) -> None:
    if len(account_number) != 20:
        raise ValidationError("Account number should have length 20")


def is_account_number_clear(account_number: str) -> None:
    if not account_number.isdigit():
        raise ValidationError(
            "Account number should not have letters or any non-digit symbols")


def is_balance_positive(balance: float) -> None:
    if balance < 0:
        raise ValidationError("Balance should be positive ")


class Account(models.Model):
    user = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE)
    account_number = models.CharField(max_length=20, unique=True,
                                      validators=[
                                          is_account_number_length_valid,
                                          is_account_number_clear,
                                      ])
    balance = models.DecimalField(decimal_places=2, max_digits=200, default=0,
                                  validators=[is_balance_positive])

    def __str__(self):
        return f'Account: {self.account_number}; User: {self.user.name}'

    class Meta:
        verbose_name = 'account'
        verbose_name_plural = 'Accounts'
