from django.db import models
from django.utils import timezone

from app.models import Account


class AccountTransaction(models.Model):
    date = models.DateTimeField(default=timezone.now, blank=False)
    account_from = models.ForeignKey(Account, on_delete=models.CASCADE,
                                     related_name='account_from')
    account_to = models.ForeignKey(Account, on_delete=models.CASCADE,
                                   related_name='account_to')
    amount = models.DecimalField(max_digits=200, decimal_places=2, blank=False)

    def __str__(self):
        return f'{self.date}: {self.account_from} to {self.account_to}. Amount {self.amount}'

    class Meta:
        verbose_name = 'account transaction'
        verbose_name_plural = 'AccountTransactions'
