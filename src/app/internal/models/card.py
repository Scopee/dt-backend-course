from django.core.exceptions import ValidationError
from django.db import models

from app.models import Account


def luhn_checksum(card_number: str) -> int:
    def digits_of(n):
        return [int(digit) for digit in str(n)]

    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d * 2))
    return checksum % 10


def is_card_number_length_valid(card_number: str) -> None:
    if len(card_number) != 16:
        raise ValidationError("Card number should contain 16 symbols")


def is_card_number_clean(card_number: str) -> None:
    if not card_number.isdigit():
        raise ValidationError(
            "Card number should not contain letters or any non-digit symbols")


def is_card_number_luhn_valid(card_number: str) -> None:
    if luhn_checksum(card_number) != 0:
        raise ValidationError("Card number is not valid")


class Card(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    card_number = models.CharField(max_length=16, unique=True,
                                   validators=[is_card_number_clean,
                                               is_card_number_luhn_valid,
                                               is_card_number_length_valid])

    def __str__(self):
        return f'Card: {self.card_number}; User: {self.account.user.name}'

    class Meta:
        verbose_name = 'card'
        verbose_name_plural = 'Cards'
