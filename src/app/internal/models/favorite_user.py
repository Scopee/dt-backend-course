from django.db import models

from app.internal.models.user import UserInfo


class FavoriteUser(models.Model):
    user = models.ForeignKey(UserInfo, on_delete=models.CASCADE,
                             related_name="favorite_user_user")
    favorite_user = models.ForeignKey(UserInfo, on_delete=models.CASCADE,
                                      related_name="favorite_user_favorite_user")

    class Meta:
        verbose_name = 'favorite user'
        verbose_name_plural = 'Favorite Users'
        constraints = [
            models.UniqueConstraint(fields=['user', 'favorite_user'],
                                    name='unique_favorite_user')
        ]
