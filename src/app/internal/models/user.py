from django.db import models


class UserInfo(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f'{self.name}'

    def to_json(self):
        return {'name': self.name, 'phone_number': self.phone_number}

    class Meta:
        verbose_name = 'user info'
        verbose_name_plural = 'Users Info'
