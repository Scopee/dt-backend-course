from decimal import Decimal
from typing import List, Union

from django.db import IntegrityError, transaction
from django.db.models import F

from app.internal.models.account import Account
from app.internal.models.account_transaction import AccountTransaction
import logging

logger = logging.getLogger(__name__)


def get_accounts_info(user_id: int) -> List[Account]:
    return Account.objects.filter(user_id__exact=user_id).values(
        'account_number', 'balance').all()


def get_account_info(account: str) -> Union[None, Account]:
    return Account.objects.filter(account_number=account).first()


def get_account_info_by_card_number(card_number: str) -> Union[None, Account]:
    return Account.objects.filter(card__card_number=card_number).first()


def get_account_info_by_username(username: str) -> Union[None, Account]:
    return Account.objects.filter(user__name=username).first()


def send_money(user_id: int, account_from: str, to: str,
               amount: Decimal) -> bool:
    return (try_send_money_by_account_number(user_id, account_from, to, amount)
            or try_send_money_by_card_number(user_id, account_from, to, amount)
            or try_send_money_by_username(user_id, account_from, to, amount))


def try_send_money_by_account_number(user_id: int, account_number_from: str,
                                     account_number_to: str,
                                     amount: Decimal) -> bool:
    account_from = get_account_info(account_number_from)
    if not account_from:
        raise ValueError('Account not exists')
    if account_from.user.id != user_id:
        raise ValueError('You cannot send money from not yours account')

    account_to = get_account_info(account_number_to)
    if not account_to:
        return False
    make_transaction(account_from, account_to, amount)
    return True


def try_send_money_by_card_number(user_id: int, account_number_from: str,
                                  card_number_to: str,
                                  amount: Decimal) -> bool:
    account_from = get_account_info(account_number_from)
    if not account_from:
        raise ValueError('Account not exists')
    if account_from.user.id != user_id:
        raise ValueError('You cannot send money from not yours account')

    account_to = get_account_info_by_card_number(card_number_to)
    if not account_to:
        return False
    make_transaction(account_from, account_to, amount)
    return True


def try_send_money_by_username(user_id: int, account_number_from: str,
                               username: str,
                               amount: Decimal) -> bool:
    account_from = get_account_info(account_number_from)
    if not account_from:
        raise ValueError('Account not exists')
    if account_from.user.id != user_id:
        raise ValueError('You cannot send money from not yours account')

    account_to = get_account_info_by_username(username)
    if not account_to:
        return False
    make_transaction(account_from, account_to, amount)
    return True


@transaction.atomic
def make_transaction(account_from: Account, account_to: Account,
                     amount: Decimal):
    try:
        if account_from.balance < amount:
            raise IntegrityError()
        account_from.balance = F('balance') - amount
        account_to.balance = F('balance') + amount
        account_from.save(update_fields=['balance'])
        account_to.save(update_fields=['balance'])
        AccountTransaction.objects.create(account_from=account_from,
                                          account_to=account_to, amount=amount)
    except IntegrityError:
        account_from.balance += amount
        account_to.balance -= amount
        raise
