from typing import List

from django.db.models import Q

from app.internal.models.account_transaction import AccountTransaction
from app.internal.services.account_service import \
    get_account_info_by_card_number


def get_transactions(number: str) -> List[AccountTransaction]:
    if len(number) != 16 and len(number) != 20:
        raise ValueError('Incorrect number value. Should be 16 or 20')
    if len(number) == 16:
        return _get_transactions_by_card_number(number)
    return _get_transactions_by_account(number)


def get_user_interacted(user_id: id) -> List[str]:
    transactions = AccountTransaction.objects.filter(
        Q(account_to__user_id=user_id) | Q(
            account_from__user_id=user_id)).values('account_to__user_id',
                                                   'account_from__user__name',
                                                   'account_to__user__name').all()
    return list(
        set(map(lambda x: _get_interacted_user(x, user_id), transactions)))


def _get_interacted_user(transaction: dict,
                         user_id: int) -> str:
    if transaction['account_to__user_id'] == user_id:
        return transaction['account_from__user__name']
    return transaction['account_to__user__name']


def _get_transactions_by_account(account_number: str) \
        -> List[AccountTransaction]:
    return AccountTransaction.objects.filter(
        Q(account_to__account_number=account_number) | Q(
            account_from__account_number=account_number))


def _get_transactions_by_card_number(card_number: str) -> List[AccountTransaction]:
    account = get_account_info_by_card_number(card_number)
    return _get_transactions_by_account(account.account_number)
