from typing import List

from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.card import Card


def get_user_cards(user_id: int) -> List[Card]:
    return Card.objects.filter(account__user_id=user_id).values(
        'card_number').all()


def get_user_card(user_id: int, card_number: str) -> Card:
    try:
        card = Card.objects.get(card_number=card_number)
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist('Card does not exists')

    if card.account.user.id == user_id:
        return card
    raise ObjectDoesNotExist('Card does not exists')
