from typing import List

from django.db.models import ObjectDoesNotExist

from app.internal.models.favorite_user import FavoriteUser
from app.internal.models.user import UserInfo


def add_user_to_favorite(user: UserInfo, user_to: UserInfo) -> FavoriteUser:
    return FavoriteUser.objects.create(user=user, favorite_user=user_to)


def remove_user_from_favorites(user_id: int, user_to: str):
    try:
        fav = FavoriteUser.objects.get(user__id=user_id,
                                       favorite_user__name=user_to)
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist('User not in your favorites')

    fav.delete()


def get_all_favorite_users(user_id: int) -> List[dict]:
    return FavoriteUser.objects.filter(user__id=user_id).values(
        'favorite_user__name').all()
