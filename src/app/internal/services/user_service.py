from typing import Union

from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import now

from app.internal.models.user import UserInfo


def get_user_info(user_id: int) -> Union[None, UserInfo]:
    try:
        return UserInfo.objects.get(id__exact=user_id)
    except ObjectDoesNotExist:
        return None


def get_user_info_by_name(username: str) -> Union[None, UserInfo]:
    return UserInfo.objects.filter(name=username).first()


def save_username(user_id: int, name: str):
    if not exists(user_id):
        UserInfo(id=user_id, name=name, phone_number=None).save()


def exists(user_id: int) -> bool:
    return UserInfo.objects.filter(id=user_id).exists()


def save_number(user_id: int, phone_number: str):
    user = UserInfo.objects.get(id__exact=user_id)
    user.phone_number = phone_number
    user.date_update = now()
    user.save()
