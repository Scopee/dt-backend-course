from typing import Optional, Union

from telegram import Message
from telegram.ext import MessageFilter
from telegram.ext.filters import DataDict


class ArgumentFilter(MessageFilter):

    def __init__(self, count: int, message: str):
        self.count = count
        self.message = message

    def filter(self, message: Message) -> Optional[Union[bool, DataDict]]:
        if self.count != len(message.text.split()) - 1:
            message.bot.send_message(chat_id=message.chat_id,
                                     text=f'''Argument count mismatch.
{self.message}''')
            return False
        return True
