from typing import Optional, Union

from telegram import Message
from telegram.ext import MessageFilter
from telegram.ext.filters import DataDict

from app.internal.services.user_service import get_user_info
from app.internal.transport.bot.telegram_utils import Consts


class IsPhoneFilled(MessageFilter):
    def filter(self, message: Message) -> Optional[Union[bool, DataDict]]:
        user = get_user_info(message.from_user.id)
        if not user:
            message.bot.send_message(chat_id=message.chat_id,
                                     text=Consts.USER_NOT_FOUND)
            return False
        if not user.phone_number:
            message.bot.send_message(chat_id=message.chat_id,
                                     text=Consts.PHONE_NOT_FILLED)
            return False
        return bool(user.phone_number)
