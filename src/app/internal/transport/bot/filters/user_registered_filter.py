from typing import Optional, Union

from telegram import Message
from telegram.ext import MessageFilter
from telegram.ext.filters import DataDict

import app.internal.services.user_service as user_service
from app.internal.transport.bot.telegram_utils import Consts


class UserRegisteredFilter(MessageFilter):
    def filter(self, message: Message) -> Optional[Union[bool, DataDict]]:
        user_id = message.from_user.id
        user = user_service.get_user_info(user_id)
        if not user:
            message.bot.send_message(
                chat_id=message.chat_id,
                message=Consts.USER_NOT_FOUND
            )
            return False
        return True
