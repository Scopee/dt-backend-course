import logging
from decimal import Decimal

from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import CallbackContext
from telegram.utils.helpers import escape_markdown

import app.internal.services.account_service as account_service
import \
    app.internal.services.account_transaction_service as account_transaction_service
import app.internal.services.cards_service as cards_service
import app.internal.services.favorite_user_service as favorite_user_service
import app.internal.services.user_service as user_service
from app.internal.transport.bot.telegram_utils import (
    Consts,
    format_card,
    format_cards,
    format_favorite_users,
    format_transactions,
    format_user_accounts,
    format_user_info,
    format_users_interacted,
    get_user_id,
    get_username,
    log_request,
    send_message,
)

logger = logging.getLogger(__name__)


def start_callback(update: Update, context: CallbackContext) -> None:
    log_request('/start')
    name = get_username(update)
    user_id = get_user_id(update)
    if user_service.exists(user_id):
        send_message(context, update, Consts.USER_ALREADY_STARTED)
        return
    user_service.save_username(user_id, name)
    send_message(context, update, Consts.WELCOME)


def set_phone_callback(update: Update, context: CallbackContext) -> None:
    log_request('/set_phone')
    keyboard = [
        [KeyboardButton('Share phone number', request_contact=True)]
    ]
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    update.message.reply_text(
        escape_markdown(Consts.SHARE_PHONE,
                        version=2),
        reply_markup=reply_markup)


def contact_button_callback(update: Update, context: CallbackContext) -> None:
    log_request('contact button')
    query = update.effective_message.contact
    phone_number = query.phone_number
    user_id = get_user_id(update)
    user_service.save_number(user_id, phone_number)
    send_message(context, update, Consts.PHONE_SAVED)


def me_callback(update: Update, context: CallbackContext) -> None:
    log_request('/me')
    user_id = get_user_id(update)
    user = user_service.get_user_info(user_id)

    send_message(context, update,
                 format_user_info(user.name, user.phone_number))


def account_callback(update: Update, context: CallbackContext) -> None:
    log_request('/account')
    user_id = get_user_id(update)
    accounts = account_service.get_accounts_info(user_id)
    send_message(context, update, format_user_accounts(accounts))


def cards_callback(update: Update, context: CallbackContext) -> None:
    log_request('/cards')
    user_id = get_user_id(update)
    cards = cards_service.get_user_cards(user_id)
    send_message(context, update,
                 escape_markdown(format_cards(cards), version=2))


def card_balance_callback(update: Update, context: CallbackContext) -> None:
    log_request('/card_balance')
    card_number = context.args[0]
    user_id = get_user_id(update)
    try:
        card = cards_service.get_user_card(user_id, card_number)
    except ObjectDoesNotExist as e:
        send_message(context, update, str(e))
        return
    send_message(context, update, format_card(card))


def send_money_callback(update: Update, context: CallbackContext) -> None:
    log_request('/send_money')
    user_id = get_user_id(update)
    account_from = context.args[0]
    to = context.args[1]

    try:
        amount = Decimal(context.args[2])
    except ValueError:
        send_message(context, update, Consts.AMOUNT_SHOULD_BE_DECIMAL)
        return

    try:
        success = account_service.send_money(user_id, account_from, to, amount)
    except IntegrityError:
        send_message(context, update, Consts.UNABLE_TO_SEND_MONEY)
        return
    except ValueError as e:
        send_message(context, update, str(e))
        return

    if success:
        send_message(context, update, Consts.SEND_SUCCESSFUL)
    else:
        send_message(context, update, Consts.UNABLE_TO_SEND_MONEY)


def get_favorite_users(update: Update, context: CallbackContext) -> None:
    log_request('/favorites')
    user_id = get_user_id(update)
    favorite_users = favorite_user_service.get_all_favorite_users(user_id)
    send_message(context, update,
                 escape_markdown(format_favorite_users(favorite_users),
                                 version=2))


def add_user_to_favorite(update: Update, context: CallbackContext) -> None:
    log_request('/add_to_favorite')
    user_id = get_user_id(update)
    user_from = user_service.get_user_info(user_id)
    user_to_name = context.args[0]
    user_to = user_service.get_user_info_by_name(user_to_name)
    if not user_from or not user_to:
        send_message(context, update, 'User not found')
        return
    favorite_user_service.add_user_to_favorite(user_from, user_to)
    send_message(context, update, 'Added successfully')


def remove_user_from_favorites(update: Update,
                               context: CallbackContext) -> None:
    log_request('/remove_from_favorite')
    user_id = get_user_id(update)
    user_to_name = context.args[0]
    try:
        favorite_user_service.remove_user_from_favorites(user_id, user_to_name)
    except ObjectDoesNotExist as e:
        send_message(context, update, str(e))
        return
    send_message(context, update, 'Removed successfully')


def get_transaction_by_account_or_card(update: Update,
                                       context: CallbackContext) -> None:
    log_request('/get_transactions')
    number = context.args[0]
    try:
        transactions = account_transaction_service.get_transactions(number)
    except ValueError as e:
        send_message(context, update, str(e))
        return
    send_message(context, update,
                 escape_markdown(format_transactions(transactions), version=2))


def get_users_interacted(update: Update, contex: CallbackContext) -> None:
    log_request('/get_users_interacted')
    user_id = get_user_id(update)
    users_interacted = account_transaction_service.get_user_interacted(user_id)
    send_message(contex, update,
                 escape_markdown(format_users_interacted(users_interacted),
                                 version=2))


def show_help(update: Update, context: CallbackContext) -> None:
    log_request('/help')
    user_id = get_user_id(update)
    user = user_service.get_user_info(user_id)
    if user:
        send_message(context, update, Consts.REGISTERED_USER_HELP)
    else:
        send_message(context, update, Consts.UNREGISTERED_USER_HELP)
