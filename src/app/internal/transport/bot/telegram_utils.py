import logging
from typing import List

from telegram import ParseMode, Update
from telegram.ext import CallbackContext
from telegram.utils.helpers import escape_markdown

from app.internal.models.account_transaction import AccountTransaction
from app.internal.models.card import Card


class Consts:
    PHONE_SAVED = 'Phone number saved successfully'
    PHONE_NOT_FILLED = 'Please fill your number before using bot'
    SHARE_PHONE = 'Would you mind sharing your contact with?'
    WELCOME = escape_markdown('''Welcome!
Please fill your number with /set_phone''', version=2)
    USER_NOT_FOUND = escape_markdown('Please start using bot with /start',
                                     version=2)

    USER_ALREADY_STARTED = 'You are already registered!'

    AMOUNT_SHOULD_BE_DECIMAL = 'Amount should be decimal'

    UNABLE_TO_SEND_MONEY = 'Unable to send money'
    SEND_SUCCESSFUL = 'Money sent successfully'

    REGISTERED_USER_HELP = escape_markdown('''Available commands:
- /help - show this message
- /me - show information about you
- /set_phone - save your phone number
- /accounts - show your accounts
- /cards - show your cards
- /card_balance <card_number> - show balance of card
- /send_money <your_account_number> <account_number/card_number/username_to_send> <amount> - send money to someone
- /favorites - show your favorite users
- /add_to_favorites <username> - add user to favorites
- /remove_from_favorites <username> - remove user from favorite
- /get_transactions <account_number/card_number> - show transactions by card or account
- /get_users_interacted - show usernames of people you have been interacted
    ''', version=2)

    UNREGISTERED_USER_HELP = escape_markdown('''
Available commands:
- /help - show this message
- /start - start using bot
- /set_phone - save your phone number
    ''', version=2)


logger = logging.getLogger(__name__)


def send_message(context: CallbackContext, update: Update, text: str) -> None:
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=text,
                             parse_mode=ParseMode.MARKDOWN_V2)


def format_user_info(name: str, phone_number: str) -> str:
    return f'''
    *Username:* {escape_markdown(name, version=2)}
*Phone number:* {escape_markdown(phone_number, version=2)}
    '''


def format_user_accounts(accounts: List[dict]) -> str:
    if not accounts:
        return "You have 0 accounts"
    return "Your accounts:\n" + '\n\n'.join(map(_format_account, accounts))


def format_card(card: Card) -> str:
    return f'''*Account*: {escape_markdown(_split_card_number(card.account.account_number), version=2)}
*Card Number*: {escape_markdown(_split_card_number(card.card_number), version=2)}
*Balance*: {escape_markdown(str(card.account.balance), version=2)}'''


def format_cards(cards: List[dict]) -> str:
    if not cards:
        return 'You don\'t have any cards'
    return 'Your cards:\n' + '\n'.join(
        [
            f'{i + 1}. {escape_markdown(_split_card_number(card["card_number"]), version=2)}'
            for i, card in
            enumerate(cards)])


def _split_card_number(card_number: str) -> str:
    res = ''
    for i in range(0, len(card_number), 4):
        res += card_number[i:i + 4] + ' '
    return res


def _format_account(account: dict) -> str:
    return f'''*Account*: {escape_markdown(_split_card_number(account['account_number']), version=2)}
*Balance:* {escape_markdown(str(account['balance']), version=2)}'''


def format_favorite_users(favorite_users: List[dict]) -> str:
    if not favorite_users:
        return 'You don\'t have favorite users'
    return 'Your favorite users:\n' + '\n'.join(
        [f'{i + 1}. {_format_favorite_user(user)}'
         for i, user in enumerate(favorite_users)])


def _format_favorite_user(user: dict) -> str:
    return f'{user["favorite_user__name"]}'


def format_transactions(transactions: List[AccountTransaction]) -> str:
    if not transactions:
        return 'You don\'t have any transactions'
    return 'Your transactions:\n' + '\n'.join(
        str(transaction) for transaction in transactions)


def format_users_interacted(users: List[str]) -> str:
    if not users:
        return 'You have not been interacted with any user'
    return 'Users you have been interacted with:\n' + '\n'.join(users)


def get_user_id(update: Update) -> int:
    return update.effective_user.id


def get_username(update: Update) -> str:
    return update.effective_user.name[1:]


def log_request(handler: str) -> None:
    logger.info(f'Got telegram request on handler {handler}')
