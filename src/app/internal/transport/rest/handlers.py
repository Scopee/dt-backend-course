import logging

from django.http import JsonResponse
from django.views import View

from app.internal.services.user_service import get_user_info_by_name

NOT_FOUND_OBJECT = JsonResponse({'message': 'User not found', 'code': 404},
                                status=404)


class UserInfoView(View):
    logger = logging.getLogger(__name__)

    def get(self, request, name: str) -> JsonResponse:
        self.logger.info('Got http request on handler /api/me')
        user = get_user_info_by_name(name)
        if not user:
            self.logger.warning(f'User {name} not found')
            return NOT_FOUND_OBJECT
        return JsonResponse(user.to_json())
