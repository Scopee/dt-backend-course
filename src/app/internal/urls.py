from django.urls import path

from app.internal.transport.rest.handlers import UserInfoView

urlpatterns = [
    path('me/<str:name>/', UserInfoView.as_view(), name="me")
]
