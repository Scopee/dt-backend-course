from django.conf import settings
from django.core.management.base import BaseCommand

from app.internal.bot import BotListener


class Command(BaseCommand):
    def handle(self, *args, **options):
        BotListener.run_pooling(settings.TELEGRAM_TOKEN)
