from django.conf import settings
from django.core.management.base import BaseCommand

from app.internal.bot import BotListener


class Command(BaseCommand):
    def handle(self, *args, **options):
        BotListener.run_webhook(settings.TELEGRAM_TOKEN, settings.WEBHOOK_IP,
                                settings.WEBHOOK_PORT,
                                settings.WEBHOOK_URL_PATH,
                                settings.WEBHOOK_URL)
