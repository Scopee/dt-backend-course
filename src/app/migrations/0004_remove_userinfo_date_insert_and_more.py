# Generated by Django 4.0.4 on 2022-05-11 17:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_accounttransaction'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userinfo',
            name='date_insert',
        ),
        migrations.RemoveField(
            model_name='userinfo',
            name='date_update',
        ),
    ]
