import pytest

from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.models.user import UserInfo


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_user():
    user = UserInfo.objects.create(id=2, name="test",
                                   phone_number="+79999999991")
    return user


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_user_2():
    user = UserInfo.objects.create(id=3, name="test2",
                                   phone_number="+79999999992")
    return user


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_user_3():
    user = UserInfo.objects.create(id=4, name="test3",
                                   phone_number="+79999999993")
    return user


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_bank_account(test_user: UserInfo):
    bank_account = Account.objects.create(user=test_user,
                                          account_number="11111111111111111111",
                                          balance=10)
    return bank_account


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_bank_account_2(test_user_2: UserInfo):
    bank_account = Account.objects.create(
        user=test_user_2, account_number="99999999999999999999",
        balance=10
    )
    return bank_account


@pytest.mark.django_db
@pytest.fixture(scope="function")
def test_card(test_bank_account: Account):
    card = Card.objects.create(account=test_bank_account,
                               card_number="4539149464897633")
    return card
