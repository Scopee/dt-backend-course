import pytest
from django.urls import reverse


@pytest.mark.django_db
@pytest.mark.integration
def test_user_api_success(client, test_user):
    api = reverse("me", kwargs={"name": test_user.name})
    response = client.get(api)
    assert response.json() == {
        "name": test_user.name,
        "phone_number": test_user.phone_number,
    }


@pytest.mark.django_db
@pytest.mark.integration
def test_user_api_404(client, test_user):
    api = reverse("me", kwargs={"name": test_user.name + '123'})
    response = client.get(api)
    assert response.status_code == 404
