import pytest
from django.urls import reverse


@pytest.mark.django_db
@pytest.mark.smoke
def test_user_api_success(client, test_user):
    api = reverse("me", kwargs={"name": test_user.name})
    response = client.get(api)
    assert response.status_code == 200
