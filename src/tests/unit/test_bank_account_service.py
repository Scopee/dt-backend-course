from decimal import Decimal

import pytest
from django.db import IntegrityError

import app.internal.services.account_service as account_service
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.models.user import UserInfo

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestBankAccountServices:
    pytestmark = pytest.mark.django_db

    def test_get_bank_accounts(self, test_user: UserInfo,
                               test_bank_account: Account):
        bank_account = account_service.get_accounts_info(test_user.id)
        assert list(bank_account) == [{
            'account_number': test_bank_account.account_number,
            'balance': test_bank_account.balance}]

    def test_get_bank_account_by_account_id(self, test_user: UserInfo,
                                            test_bank_account: Account):
        bank_account = account_service.get_account_info(
            test_bank_account.account_number)
        assert isinstance(bank_account, Account)
        assert bank_account == test_bank_account

    def test_get_bank_account_by_account_id_not_found(self,
                                                      test_user: UserInfo,
                                                      test_bank_account: Account):
        bank_account = account_service.get_account_info("1234567812345678")
        assert bank_account is None

    def test_get_bank_account_from_username(self, test_user: UserInfo,
                                            test_bank_account: Account):
        bank_account = account_service.get_account_info_by_username(
            test_user.name)
        assert isinstance(bank_account, Account)
        assert bank_account.user == test_user

    def test_get_bank_account_from_username_not_found(self,
                                                      test_user: UserInfo,
                                                      test_bank_account: Account):
        bank_account = account_service.get_account_info_by_username("no")
        assert bank_account is None

    def test_get_bank_account_from_card(self,
                                        test_bank_account: Account,
                                        test_card: Card):
        bank_account = account_service.get_account_info_by_card_number(
            test_card.card_number)
        assert isinstance(bank_account, Account)
        assert test_bank_account == bank_account

    def test_get_bank_account_from_card_not_found(self,
                                                  test_card: Card):
        bank_account = account_service.get_account_info_by_card_number(
            "1234567812345678")
        assert bank_account is None

    def test_send_money_by_username(
            self, test_user: UserInfo, test_user_2,
            test_bank_account: Account, test_bank_account_2
    ):
        is_send = account_service.try_send_money_by_username(
            test_user.id,
            test_bank_account.account_number,
            test_user_2.name,
            amount=Decimal(1))
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 9
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 11

    def test_send_money_by_username_unsuccess(
            self, test_user: UserInfo, test_user_2,
            test_bank_account: Account, test_bank_account_2
    ):
        is_send = account_service.try_send_money_by_username(
            test_user.id,
            test_bank_account.account_number,
            "no", Decimal(1))
        assert not is_send

    def test_send_money_by_card_number(
            self, test_card: Card, test_user_2,
            test_bank_account: Account, test_bank_account_2
    ):
        is_send = account_service.try_send_money_by_card_number(
            test_user_2.id,
            test_bank_account_2.account_number,
            test_card.card_number, Decimal(1))
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 11
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 9

    def test_send_money_by_card_number_unsuccess(self,
                                                 test_user_2: UserInfo,
                                                 test_bank_account_2: Account):
        is_send = account_service.try_send_money_by_card_number(
            test_user_2.id,
            test_bank_account_2.account_number,
            "1234567812345678", Decimal(1))
        assert not is_send

    def test_send_money_by_bank_account_number(
            self, test_bank_account: Account, test_bank_account_2,
            test_user: UserInfo
    ):
        is_send = account_service.try_send_money_by_account_number(
            test_user.id, test_bank_account.account_number,
            test_bank_account_2.account_number,
            Decimal(1)
        )
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 9
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 11

    def test_send_money_by_bank_account_number_unsuccess(
            self, test_user: UserInfo, test_bank_account: Account):
        is_send = account_service.try_send_money_by_account_number(
            test_user.id,
            test_bank_account.account_number,
            "1234567891234567899",
            Decimal(1))
        assert not is_send

    def test_send_money_username(
            self, test_user: UserInfo, test_user_2,
            test_bank_account: Account, test_bank_account_2
    ):
        is_send = account_service.send_money(test_user.id,
                                             test_bank_account.account_number,
                                             test_user_2.name,
                                             Decimal(1))
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 9
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 11

    def test_send_money_card_number(
            self, test_user_2, test_card: Card,
            test_bank_account: Account, test_bank_account_2
    ):
        is_send = account_service.send_money(test_user_2.id,
                                             test_bank_account_2.account_number,
                                             test_card.card_number,
                                             Decimal(1))
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 11
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 9

    def test_send_money_account_number(
            self, test_user: UserInfo, test_bank_account_2,
            test_bank_account: Account
    ):
        is_send = account_service.send_money(test_user.id,
                                             test_bank_account.account_number,
                                             test_bank_account_2.account_number,
                                             Decimal(1))
        assert is_send
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 9
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 11

    def test_send_money_unsuccess(self,
                                  test_user: UserInfo,
                                  test_bank_account: Account):
        is_send = account_service.send_money(test_user.id,
                                             test_bank_account.account_number,
                                             "1o1", Decimal(1))
        assert not is_send

    def test_transfer(self, test_bank_account: Account,
                      test_bank_account_2):
        account_service.make_transaction(test_bank_account,
                                         test_bank_account_2,
                                         amount=Decimal(1))
        assert Account.objects.get(
            account_number=test_bank_account.account_number).balance == 9
        assert Account.objects.get(
            account_number=test_bank_account_2.account_number).balance == 11

    def test_transfer_unsuccess(self,
                                test_bank_account: Account,
                                test_bank_account_2):
        with pytest.raises(IntegrityError):
            account_service.make_transaction(test_bank_account,
                                             test_bank_account_2,
                                             Decimal(999))
