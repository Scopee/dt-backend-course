import pytest

import app.internal.services.cards_service as card_service
from app.internal.models.card import Card
from app.internal.models.user import UserInfo


@pytest.mark.django_db
class TestCardServices:
    pytestmark = pytest.mark.django_db

    def test_get_card_success(self, test_user: UserInfo, test_card: Card):
        card = card_service.get_user_card(test_user.id, test_card.card_number)
        assert card == test_card

    def test_get_cards_success(self, test_user: UserInfo, test_card: Card):
        cards = card_service.get_user_cards(test_user.id)
        assert list(cards) == [{'card_number': test_card.card_number}]
