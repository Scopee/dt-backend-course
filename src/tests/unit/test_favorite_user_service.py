import pytest
from django.core.exceptions import ObjectDoesNotExist

import app.internal.services.favorite_user_service as favorite_user_service
from app.internal.models.favorite_user import FavoriteUser
from app.internal.models.user import UserInfo


@pytest.mark.django_db
class TestFavoriteUserService:
    pytestmark = pytest.mark.django_db

    def test_add_user_to_favorite(self, test_user: UserInfo,
                                  test_user_2: UserInfo):
        favorite_user_added = favorite_user_service.add_user_to_favorite(
            test_user, test_user_2)
        favorite_user_from_db = FavoriteUser.objects.filter(
            favorite_user__id=3).first()
        assert favorite_user_added == favorite_user_from_db

    def test_remove_user_from_favorite(self, test_user: UserInfo,
                                       test_user_2):
        favorite_usr = FavoriteUser(user=test_user,
                                    favorite_user=test_user_2)
        favorite_usr.save()
        favorite_user_service.remove_user_from_favorites(test_user.id,
                                                         test_user_2.name)
        assert len(FavoriteUser.objects.all()) == 0

    def test_delete_user_from_favorite_not_found(self,
                                                 test_user: UserInfo,
                                                 test_user_2: UserInfo):
        with pytest.raises(
                ObjectDoesNotExist,
                match='User not in your favorites'
        ):
            favorite_user_service.remove_user_from_favorites(test_user.id,
                                                             test_user_2.name)

    def test_get_favorite_users(self, test_user: UserInfo,
                                test_user_2: UserInfo):
        favorite_usr = FavoriteUser(user=test_user,
                                    favorite_user=test_user_2)
        favorite_usr.save()
        favorite_users = favorite_user_service.get_all_favorite_users(
            test_user.id)
        assert len(favorite_users) == 1
