import pytest

import app.internal.services.user_service as user_service
from app.internal.models.user import UserInfo


@pytest.mark.django_db
class TestUserService:
    pytestmark = pytest.mark.django_db

    def test_create_user(self):
        user_service.save_username(1, 'test')
        user_from_db = UserInfo.objects.filter(id=1).first()
        assert 1 == user_from_db.id

    def test_set_phone(self, test_user: UserInfo):
        user_service.save_number(test_user.id, "+79999999991")
        user_from_db = UserInfo.objects.filter(id=test_user.id).first()
        assert test_user.phone_number == user_from_db.phone_number

    def test_get_user_success(self, test_user: UserInfo):
        get_usr = user_service.get_user_info(test_user.id)
        assert get_usr == test_user

    def test_get_user_not_found(self):
        get_usr = user_service.get_user_info(3)
        assert get_usr is None

    def test_get_user_from_username(self, test_user: UserInfo):
        get_usr = user_service.get_user_info_by_name(test_user.name)
        assert get_usr == test_user

    def test_get_user_from_username_not_found(self):
        get_usr = user_service.get_user_info_by_name("no")
        assert get_usr is None
